import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AuthServices {
  static final FirebaseAuth _auth = FirebaseAuth.instance;

  
   static User? getCurrentUser() {
    return _auth.currentUser;
  }

  static String? getCurrentUserUID(){
    return _auth.currentUser?.uid;
  }
  

  static Future<UserCredential> registerWithEmail(String email, String password, String name, String lastName) async {
    return await _auth.createUserWithEmailAndPassword(email: email, password: password);
  }

  static Future<UserCredential> signInWithEmail(String email, String password) async {
    return await _auth.signInWithEmailAndPassword(email: email, password: password);
  }

  static Future<void> signOut() async {
    return await _auth.signOut();
  }

  static Future<void> sendPasswordResetEmail(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }
  
  static Future<void> showResetPasswordDialog(BuildContext context) async {
    final emailController = TextEditingController();
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Reset Password'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                const Text('Enter your email to reset your password.'),
                TextField(
                  controller: emailController,
                  decoration: const InputDecoration(labelText: 'Email'),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Send'),
              onPressed: () async {
                if (emailController.text.isNotEmpty) {
                  await _auth.sendPasswordResetEmail(email: emailController.text);
                  Navigator.of(context).pop();
                  // Afficher un message de confirmation ou gérer les erreurs
                }
              },
            ),
          ],
        );
      },
    );
  }

  static Future<void> updateEmail(String newEmail) async {
    User? user = _auth.currentUser;
    if (user != null && newEmail.isNotEmpty) {
      await user.updateEmail(newEmail);
    }
  }

  static Future<void> updatePassword(String newPassword) async {
    User? user = _auth.currentUser;
    if (user != null && newPassword.isNotEmpty) {
      await user.updatePassword(newPassword);
    }
  }

  static Future<void> updateProfile(String displayName, String photoURL) async {
    User? user = _auth.currentUser;
    if (user != null) {
      await user.updateProfile(displayName: displayName, photoURL: photoURL);
    }
  }

  static Future<void> deleteAccount() async {
    User? user = _auth.currentUser;
    if (user != null) {
      await user.delete();
    }
  }
}



 


 


  

