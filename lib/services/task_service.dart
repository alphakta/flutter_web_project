import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_web_project/models/task.dart';

class TaskService {
  final CollectionReference taskCollection =
      FirebaseFirestore.instance.collection('tasks');

  Future<void> createTask(Task task) async {
    DocumentReference documentReference =
        await taskCollection.add(task.toJson());
    String taskId = documentReference.id;

    await taskCollection.doc(taskId).update({'id': taskId});
  }

  Future<List<Task>> getAllTasks() async {
    QuerySnapshot querySnapshot = await taskCollection.get();

    List<Task> tasks = querySnapshot.docs.map((DocumentSnapshot document) {
      Map<String, dynamic> data = document.data() as Map<String, dynamic>;
      Task task = Task(
        title: data['title'] ?? '',
        description: data['description'] ?? '',
        status: data['status'] ?? '',
        assignedUserId: data['assignedUserId'] ?? '',
        id: data['id'] ?? '',
      );
      return task;
    }).toList();
    return tasks;
  }

  Future<List<Task>> getTasksForUser(String userId) async {
    QuerySnapshot querySnapshot =
        await taskCollection.where('assignedUserId', isEqualTo: userId).get();

    List<Task> tasks = querySnapshot.docs.map((DocumentSnapshot document) {
      Map<String, dynamic> data = document.data() as Map<String, dynamic>;
      Task task = Task(
        title: data['title'] ?? '',
        description: data['description'] ?? '',
        status: data['status'] ?? '',
        assignedUserId: data['assignedUserId'] ?? '',
        id: data['id'] ?? '',
      );
      return task;
    }).toList();
    return tasks;
  }

  Future<void> updateTask(Task task) async {
    await taskCollection.doc(task.id).update(task.toJson());
  }

  Future<void> updateTaskStatus(String taskId, String newStatus) async {
    await taskCollection.doc(taskId).update({'status': newStatus});
  }

  Future<void> deleteTask(String taskId) async {
    await  taskCollection.doc(taskId).delete();
  }

}
