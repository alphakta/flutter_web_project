import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_web_project/models/user.dart';

class UserService {
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('users');

  Future<void> createUser(User user) async {
    await userCollection.doc(user.userID).set(user.toJson());
  }

  Future<User> getUserById(String userID) async {
    DocumentSnapshot userDoc = await userCollection.doc(userID).get();
    return User(
      name: userDoc['name'],
      lastName: userDoc['lastName'],
      userID: userDoc['userID'],
      isAdmin: userDoc['isAdmin'],
    );
  }

  Future<List<User>> getAllUsers() async {
    QuerySnapshot users = await userCollection.get();

    List<User> userList = await Future.wait(
      users.docs.map((user) async {
        return User.fromJson(user.data() as Map<String, dynamic>);
      }),
    );

    return userList;
  }

  Future<bool> getUserRole(String userID) async {
    DocumentSnapshot userDoc = await userCollection.doc(userID).get();
    return userDoc['isAdmin'];
  }

  getUserInitials(String userID) async {
    DocumentSnapshot userDoc = await userCollection.doc(userID).get();
    String name = userDoc['name'];
    String lastName = userDoc['lastName'];
    String initials = name[0] + lastName[0];
    return initials;
  }
}
