import 'package:flutter/material.dart';
import 'package:flutter_web_project/screens/auth/singnup_screen.dart';
import 'package:flutter_web_project/screens/auth/auth_screen.dart';
import 'package:flutter_web_project/screens/auth/login_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_web_project/screens/board/board_screen.dart';
import 'package:flutter_web_project/screens/create_task/create_task_screen.dart';
import 'package:flutter_web_project/screens/profile/userProfile_screen.dart';
import 'package:flutter_web_project/screens/splash-screen/splash-screen.dart';
import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo List',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue),
      home: SplashScreen(),
      routes: {
        '/auth': (context) => LoginScreen(),
        '/board': (context) => const BoardScreen(),
        '/signup': (context) => SignUpScreen(),
        '/create-task': (context) => CreateTaskScreen(),
        '/profile': (context) => const UserProfileScreen(),
      },
    );
  }
}
