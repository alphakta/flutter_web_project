import 'package:flutter/material.dart';
import 'package:flutter_web_project/services/auth_services.dart'; // Votre chemin d'accès peut varier
import 'package:firebase_auth/firebase_auth.dart';

class UserProfileScreen extends StatefulWidget {
  const UserProfileScreen({Key? key}) : super(key: key);

  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  final emailController = TextEditingController();
  final nameController = TextEditingController();
  final lastNameController = TextEditingController();

  User? user = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    super.initState();
    emailController.text = user?.email ?? '';
    // Ici, vous pourriez également initialiser les contrôleurs pour le nom et le prénom
  }

  void showChangePasswordDialog() {
    // Implémentez la logique pour afficher un dialogue de changement de mot de passe
  }

  void showChangeEmailDialog() {
    // Implémentez la logique pour afficher un dialogue de changement d'email
  }

  void signOut() async {
    await AuthServices.signOut();
    Navigator.of(context).pushReplacementNamed('/'); // Rediriger vers l'écran principal
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User Profile'),
        backgroundColor: Colors.black,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: ListView(
          children: [
            CircleAvatar(
              radius: 40,
              backgroundColor: Colors.grey, // Mettez ici l'image de l'avatar de l'utilisateur
            ),
            const SizedBox(height: 20),
            TextField(
              controller: nameController,
              decoration: const InputDecoration(labelText: 'Name'),
            ),
            const SizedBox(height: 20),
            TextField(
              controller: lastNameController,
              decoration: const InputDecoration(labelText: 'Last Name'),
            ),
            const SizedBox(height: 20),
            TextField(
              controller: emailController,
              decoration: const InputDecoration(labelText: 'Email'),
              readOnly: true, // Empêche la modification directe de l'email
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: showChangeEmailDialog,
              child: const Text('Change Email'),
            ),
            ElevatedButton(
              onPressed: showChangePasswordDialog,
              child: const Text('Change Password'),
            ),
            ElevatedButton(
              onPressed: signOut,
              child: const Text('Sign Out'),
              style: ElevatedButton.styleFrom(primary: Colors.red),
            ),
          ],
        ),
      ),
    );
  }
}
