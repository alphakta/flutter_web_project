import 'package:flutter/material.dart';
import 'package:flutter_web_project/screens/auth/auth_screen.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future.delayed(
      const Duration(seconds: 1),
      () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context) => const AuthScreen(),
          ),
        );
      },
    );

    return Scaffold(
        body: Center(
          child: Image.asset(
            'assets/images/logo_transparent.png',
            width: 400.0,
            height: 400.0,
          ),
        ),
        backgroundColor: Color.fromARGB(255, 39, 0, 0));
  }
}
