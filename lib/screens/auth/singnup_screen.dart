// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_web_project/services/auth_services.dart'; // Votre chemin d'accès peut varier
// ignore: library_prefixes
import 'package:firebase_auth/firebase_auth.dart' as FirebaseAuth;
import 'package:flutter_web_project/services/user_service.dart';
import 'package:flutter_web_project/models/user.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpScreen> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  bool _isPasswordHidden = true;

   void registerUser(BuildContext context) async {
    try {
      FirebaseAuth.UserCredential userCredential =
          await AuthServices.registerWithEmail(
        emailController.text,
        passwordController.text,
        firstNameController.text,
        lastNameController.text,
      );

      if (userCredential.user != null) {
        String userID = userCredential.user!.uid;
        User newUser = User(
          name: firstNameController.text,
          lastName: lastNameController.text,
          userID: userID,
          isAdmin: false,
        );
        await UserService().createUser(newUser);
        Navigator.of(context).pushNamed('/', arguments: userID);
      }
    } on FirebaseAuth.FirebaseException catch (e) {
      String errorMessage = 'An error occurred';
      if (e.code == 'email-already-in-use') {
        errorMessage = 'This email address is already in use.';
      } else if (e.code == 'invalid-email') {
        errorMessage = 'This email address is invalid.';
      } else if (e.code == 'weak-password') {
        errorMessage = 'The password provided is too weak.';
      }
      final snackBar = SnackBar(content: Text(errorMessage));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const Icon(
              Icons.person_add_alt_1,
              size: 90,
              color: Colors.black,
            ),
            const SizedBox(height: 20),
            const Text(
              'Create your account',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 40),
            TextField(
              controller: firstNameController,
              decoration: const InputDecoration(labelText: 'First Name'),
            ),
            const SizedBox(height: 20),
            TextField(
              controller: lastNameController,
              decoration: const InputDecoration(labelText: 'Last Name'),
            ),
            const SizedBox(height: 20),
            TextField(
              controller: emailController,
              decoration: const InputDecoration(labelText: 'Email'),
            ),
            const SizedBox(height: 20),
            TextField(
              obscureText: _isPasswordHidden,
              controller: passwordController,
              decoration: InputDecoration(
                labelText: 'Password',
                suffixIcon: IconButton(
                  icon: Icon(
                    _isPasswordHidden ? Icons.visibility_off : Icons.visibility,
                  ),
                  onPressed: () {
                    setState(() {
                      _isPasswordHidden = !_isPasswordHidden;
                    });
                  },
                ),
              ),
            ),
            const SizedBox(height: 40),
            ElevatedButton(
              child: const Text('Sign Up'),
              onPressed: () {
                registerUser(context);
              },
            ),
            const SizedBox(height: 20),
            TextButton(
              child: const Text('Already have an account? Sign in'),
              onPressed: () {
                Navigator.of(context)
                    .pushNamed('/'); // Assurez-vous que cette route existe
              },
            ),
          ],
        ),
      ),
    );
  }
}
