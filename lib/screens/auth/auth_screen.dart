import "package:firebase_auth/firebase_auth.dart";
import "package:flutter/material.dart";
import 'package:flutter_web_project/screens/auth/login_screen.dart';
import "package:flutter_web_project/screens/board/board_screen.dart";
import "package:flutter_web_project/services/user_service.dart";

class AuthScreen extends StatelessWidget {
  const AuthScreen({Key? key}) : super(key: key);

  Future<bool> getAdminRole() async {
    final User? user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      return await UserService().getUserRole(user.uid);
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          // user is logged in
          if (snapshot.hasData) {
            return FutureBuilder<bool>(
              future: getAdminRole(),
              builder: (context, adminRoleSnapshot) {
                if (adminRoleSnapshot.connectionState ==
                    ConnectionState.waiting) {
                  return const CircularProgressIndicator(); // or any loading indicator
                } else {
                  return BoardScreen(isAdmin: adminRoleSnapshot.data ?? false);
                }
              },
            );
          }
          // user is not logged in
          else {
            return LoginScreen();
          }
        },
      ),
    );
  }
}
