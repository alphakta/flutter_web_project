// ignore_for_file: use_build_context_synchronously, library_prefixes, unnecessary_null_comparison

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart' as FirebaseAuth;
import 'package:flutter_web_project/screens/auth/auth_screen.dart';
import 'package:flutter_web_project/screens/board/board_screen.dart';
import 'package:flutter_web_project/services/auth_services.dart';
import 'package:flutter_web_project/services/user_service.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginScreen> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool _isPasswordHidden = true;
  bool _isLoading = false;
  bool _mounted = false;

  @override
  void initState() {
    super.initState();
    _mounted = true;
  }

  @override
  void dispose() {
    _mounted = false;
    super.dispose();
  }

  Future<void> loginUser(BuildContext context) async {
    try {
      if (!_mounted) return; // Vérifiez si le widget est toujours monté

      setState(() {
        _isLoading = true;
      });

      FirebaseAuth.UserCredential userCredential =
          await AuthServices.signInWithEmail(
        emailController.text,
        passwordController.text,
      );
    } on FirebaseAuth.FirebaseAuthException catch (e) {
      final snackBar =
          SnackBar(content: Text(e.message ?? 'An error occurred'));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } finally {
      if (_mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const Icon(
              Icons.lock_outline,
              size: 90,
              color: Colors.black,
            ),
            const SizedBox(height: 20),
            const Text(
              'Welcome back you\'ve been missed!',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 40),
            TextField(
              controller: emailController,
              decoration: const InputDecoration(labelText: 'Email'),
            ),
            const SizedBox(height: 20),
            TextField(
              obscureText: _isPasswordHidden,
              controller: passwordController,
              decoration: InputDecoration(
                labelText: 'Password',
                suffixIcon: IconButton(
                  icon: Icon(
                    _isPasswordHidden ? Icons.visibility_off : Icons.visibility,
                  ),
                  onPressed: () {
                    setState(() {
                      _isPasswordHidden = !_isPasswordHidden;
                    });
                  },
                ),
              ),
            ),
            const SizedBox(height: 40),
            ElevatedButton(
              onPressed: _isLoading ? null : () => loginUser(context),
              child: _isLoading
                  ? const CircularProgressIndicator()
                  : const Text('Sign In'),
            ),
            const SizedBox(height: 20),
            TextButton(
              child: const Text('Don\'t have an account? Sign Up'),
              onPressed: () {
                Navigator.pushNamed(context, '/signup');
              },
            ),
          ],
        ),
      ),
    );
  }
}
