import 'package:flutter/material.dart';
import 'package:flutter_web_project/services/auth_services.dart';

class Header extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final String initialsUsers;
  final bool returnToBack;

  const Header({
    Key? key,
    required this.title,
    required this.initialsUsers,
    this.returnToBack = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      automaticallyImplyLeading: returnToBack,
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed('/profile');
            },
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: Text(
                initialsUsers,
                style: const TextStyle(color: Colors.black),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
