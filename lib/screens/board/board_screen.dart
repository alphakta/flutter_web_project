// ignore_for_file: prefer_typing_uninitialized_variables, unnecessary_this

import 'package:flutter/material.dart';
import 'package:flutter_web_project/models/modelsUtils.dart';
import 'package:flutter_web_project/screens/board/column/column.dart';
import 'package:flutter_web_project/models/task.dart';
import 'package:flutter_web_project/screens/widgets/header/header.dart';
import 'package:flutter_web_project/services/auth_services.dart';
import 'package:flutter_web_project/services/task_service.dart';
import 'package:flutter_web_project/services/user_service.dart';

class BoardScreen extends StatefulWidget {
  final bool? isAdmin;

  const BoardScreen({Key? key, this.isAdmin}) : super(key: key);

  @override
  _BoardScreenState createState() => _BoardScreenState();
}

class _BoardScreenState extends State<BoardScreen> {
  late Future<List<Task>> tasksFuture =
      Future.value([]); // Initialize with an empty list
  late String userId;
  late String initials = '';

  @override
  void initState() {
    super.initState();
    _refreshData();
  }

  Future<void> _refreshData() async {
    String? userId = await AuthServices.getCurrentUserUID();

    String userInitials = await UserService().getUserInitials(userId!);

    setState(() {
      initials = userInitials;

      if (widget.isAdmin == false) {
        tasksFuture = TaskService().getTasksForUser(userId!);
      } else {
        tasksFuture = TaskService().getAllTasks();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header(
        title: 'Board',
        initialsUsers: initials,
        returnToBack: false,
      ),
      body: FutureBuilder<List<Task>>(
        future: tasksFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return const Center(
              child: Text('Erreur de chargement des tâches'),
            );
          } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return const Center(
              child: Text('Aucune tâche trouvée.'),
            );
          } else {
            List<Task> tasks = snapshot.data!;
            return Column(
              children: <Widget>[
                Expanded(
                  child: BoardColumn(
                    title: statusOptions[0],
                    bgColor: Colors.grey,
                    tasks: tasks
                        .where((task) => task.status == statusOptions[0])
                        .toList(),
                    isAdmin: widget.isAdmin ?? false,
                    initialsUsers: initials,
                  ),
                ),
                Expanded(
                  child: BoardColumn(
                    title: statusOptions[1],
                    bgColor: Colors.blue,
                    tasks: tasks
                        .where((task) => task.status == statusOptions[1])
                        .toList(),
                    isAdmin: widget.isAdmin ?? false,
                    initialsUsers: initials,
                  ),
                ),
                Expanded(
                  child: BoardColumn(
                    title: statusOptions[2],
                    bgColor: Colors.yellow,
                    tasks: tasks
                        .where((task) => task.status == statusOptions[2])
                        .toList(),
                    isAdmin: widget.isAdmin ?? false,
                    initialsUsers: initials,
                  ),
                ),
                Expanded(
                  child: BoardColumn(
                      title: statusOptions[3],
                      bgColor: Colors.green,
                      tasks: tasks
                          .where((task) => task.status == statusOptions[3])
                          .toList(),
                      isAdmin: widget.isAdmin ?? false,
                      initialsUsers: initials),
                ),
              ],
            );
          }
        },
      ),
      backgroundColor: Colors.grey[200],
      floatingActionButton: widget.isAdmin ?? false
          ? FloatingActionButton(
              onPressed: () async {
                dynamic result =
                    await Navigator.pushNamed(context, '/create-task');
                if (result is bool && result == true) {
                  _refreshData();
                }
              },
              backgroundColor: Colors.blue,
              child: const Icon(Icons.add),
            )
          : null,
    );
  }
}
