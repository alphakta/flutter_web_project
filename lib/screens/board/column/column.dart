import 'package:flutter/material.dart';
import 'package:flutter_web_project/screens/board/card/card.dart';
import 'package:flutter_web_project/models/task.dart';
import 'package:flutter_web_project/screens/task_details/task_details_screen.dart';

class BoardColumn extends StatelessWidget {
  final String title;
  final Color bgColor;
  final List<Task> tasks;
  final bool isAdmin;
  final String initialsUsers;

  const BoardColumn({
    Key? key,
    required this.title,
    required this.bgColor,
    required this.tasks,
    required this.isAdmin,
    required this.initialsUsers,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(12),
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: bgColor,
        boxShadow: const [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 2.0,
            spreadRadius: 1.0,
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              title,
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            height: 1,
            color: Colors.grey,
            margin: const EdgeInsets.symmetric(vertical: 8.0),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: tasks.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TaskDetailsScreen(
                            task: tasks[index],
                            isAdmin: isAdmin,
                            initialsUsers: initialsUsers),
                      ),
                    );
                  },
                  child: BoardCoard(label: tasks[index].title),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
