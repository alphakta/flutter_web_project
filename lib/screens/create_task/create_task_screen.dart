// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_web_project/models/modelsUtils.dart';
import 'package:flutter_web_project/models/task.dart';
import 'package:flutter_web_project/models/user.dart';
import 'package:flutter_web_project/screens/task_details/admin_tast_details.dart';
import 'package:flutter_web_project/services/task_service.dart';
import 'package:flutter_web_project/services/user_service.dart';

class CreateTaskScreen extends StatefulWidget {
  @override
  _CreateTaskScreenState createState() => _CreateTaskScreenState();
}

class _CreateTaskScreenState extends State<CreateTaskScreen> {
  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController assignedMemberController =
      TextEditingController();

  String selectedStatus = statusOptions[0];

  List<User> users = [];
  User? selectedUser;

  @override
  void initState() {
    super.initState();
    fetchUsers();
  }

  Future<void> fetchUsers() async {
    List<User> fetchedUsers = (await UserService().getAllUsers()).cast<User>();
    setState(() {
      users = fetchedUsers;
    });
  }

  bool validateFields() {
    if (titleController.text.isEmpty || descriptionController.text.isEmpty) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('Champs obligatoires'),
          content: const Text('Veuillez remplir tous les champs.'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('OK'),
            ),
          ],
        ),
      );
      return false;
    }
    return true;
  }

  Future<void> validateAndCreateTask() async {
    if (validateFields()) {
      try {
        Task newTask = Task(
          id: '',
          title: titleController.text,
          description: descriptionController.text,
          status: selectedStatus,
          assignedUserId: selectedUser!.userID,
        );

        await TaskService().createTask(newTask);
        Navigator.pop(context, true);
      } catch (e) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text('Erreur'),
            content: const Text(
                'Une erreur s\'est produite lors de la création de la tâche.'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text('OK'),
              ),
            ],
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Créer une tâche'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            AdminTaskDetails(
              titleController: titleController,
              descriptionController: descriptionController,
              selectedStatus: selectedStatus,
            ),
            const SizedBox(height: 16.0),
            DropdownButtonFormField<User>(
              value: selectedUser,
              onChanged: (User? newValue) {
                setState(() {
                  selectedUser = newValue;
                });
              },
              items: users.map((User user) {
                return DropdownMenuItem<User>(
                  value: user,
                  child: Text('${user.name} ${user.lastName}'),
                );
              }).toList(),
              decoration: const InputDecoration(
                  labelText: 'Sélectionner un utilisateur'),
            ),
            const SizedBox(height: 16.0),
            DropdownButtonFormField<String>(
              value: selectedStatus,
              onChanged: (newValue) {
                setState(() {
                  selectedStatus = newValue!;
                });
              },
              items: statusOptions
                  .map<DropdownMenuItem<String>>(
                    (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ),
                  )
                  .toList(),
              decoration: const InputDecoration(labelText: 'Statut'),
            ),
            const SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: validateAndCreateTask,
              child: const Text('Créer la tâche'),
            ),
          ],
        ),
      ),
    );
  }
}
