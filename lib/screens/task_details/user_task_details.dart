import 'package:flutter/material.dart';
import 'package:flutter_web_project/models/task.dart';

class UserTaskDetails extends StatelessWidget {
  final Task task;

  const UserTaskDetails({Key? key, required this.task}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          'Titre :',
          style: TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black87),
        ),
        const SizedBox(height: 4.0),
        Text(
          task.title,
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 16.0),
        const Text(
          'Description détaillée :',
          style: TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black87),
        ),
        const SizedBox(height: 4.0),
        Text(
          task.description,
          style: const TextStyle(fontSize: 16),
        ),
        const SizedBox(height: 16.0),
      ],
    );
  }
}
