import 'package:flutter/material.dart';

class AdminTaskDetails extends StatefulWidget {
  final TextEditingController titleController;
  final TextEditingController descriptionController;
  final String selectedStatus;

  const AdminTaskDetails({
    Key? key,
    required this.titleController,
    required this.descriptionController,
    required this.selectedStatus,
  }) : super(key: key);

  @override
  _AdminTaskDetailsState createState() => _AdminTaskDetailsState();
}

class _AdminTaskDetailsState extends State<AdminTaskDetails> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text(
          'Modification (Administrateur uniquement)',
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 8.0),
        TextFormField(
          controller: widget.titleController,
          decoration: const InputDecoration(labelText: 'Nouveau titre'),
        ),
        const SizedBox(height: 8.0),
        TextFormField(
          controller: widget.descriptionController,
          decoration: const InputDecoration(labelText: 'Nouvelle description'),
        ),
      ],
    );
  }
}
