// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_web_project/models/modelsUtils.dart';
import 'package:flutter_web_project/models/task.dart';
import 'package:flutter_web_project/screens/task_details/admin_tast_details.dart';
import 'package:flutter_web_project/screens/task_details/user_task_details.dart';
import 'package:flutter_web_project/screens/widgets/header/header.dart';
import 'package:flutter_web_project/services/task_service.dart';

class TaskDetailsScreen extends StatefulWidget {
  final Task task;
  final String initialsUsers;
  final bool isAdmin;

  TaskDetailsScreen({
    Key? key,
    required this.task,
    required this.isAdmin,
    required this.initialsUsers,
  }) : super(key: key);

  @override
  _TaskDetailsScreenState createState() => _TaskDetailsScreenState();
}

class _TaskDetailsScreenState extends State<TaskDetailsScreen> {
  late String selectedStatus;

  late TextEditingController titleController;
  late TextEditingController descriptionController;

  @override
  void initState() {
    super.initState();
    selectedStatus = widget.task.status;

    titleController = TextEditingController(text: widget.task.title);
    descriptionController =
        TextEditingController(text: widget.task.description);
  }

  Future<void> updateTask() async {
    Task updatedTask = Task(
      id: widget.task.id,
      title: titleController.text,
      description: descriptionController.text,
      status: selectedStatus,
      assignedUserId: widget.task.assignedUserId,
    );
    await TaskService().updateTask(updatedTask);
    Navigator.pushReplacementNamed(context, '/');
  }

  Future<void> deleteTask() async {
    Task deleteTask = Task(
      id: widget.task.id,
      title: titleController.text,
      description: descriptionController.text,
      status: selectedStatus,
      assignedUserId: widget.task.assignedUserId,
    );
    await TaskService().deleteTask(deleteTask.id);
    Navigator.pushReplacementNamed(context, '/');
  }

  void deleteTaskScreen(BuildContext context) {
    // Affiche un dialogue de confirmation
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Confirmer la suppression'),
          content:
              const Text('Êtes-vous sûr de vouloir supprimer cette tâche ?'),
          actions: <Widget>[
            // Bouton pour annuler l'action
            TextButton(
              child: const Text('Annuler'),
              onPressed: () {
                Navigator.of(context)
                    .pop(); // Ferme la boîte de dialogue sans rien faire
              },
            ),
            // Bouton pour confirmer la suppression
            TextButton(
              child:
                  const Text('Supprimer', style: TextStyle(color: Colors.red)),
              onPressed: () {
                deleteTask();
                Navigator.of(context)
                    .pop(); // Ferme la boîte de dialogue après la suppression
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header(
        title: "Détails d'une tâche",
        initialsUsers: widget.initialsUsers,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget.isAdmin == false) ...[
              UserTaskDetails(task: widget.task),
            ],
            if (widget.isAdmin) ...[
              AdminTaskDetails(
                titleController: titleController,
                descriptionController: descriptionController,
                selectedStatus: selectedStatus,
              ),
            ],
            const SizedBox(height: 16.0),
            Row(
              children: [
                const Text(
                  'Statut : ',
                  style: TextStyle(fontSize: 16),
                ),
                DropdownButton<String>(
                  value: selectedStatus,
                  onChanged: (String? newValue) {
                    setState(() {
                      selectedStatus = newValue!;
                    });
                  },
                  items: statusOptions.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
                const SizedBox(width: 16.0),
                ElevatedButton(
                  onPressed: () {
                    updateTask();
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue,
                    textStyle: const TextStyle(fontSize: 16),
                  ),
                  child: const Text('Valider'),
                ),
                ElevatedButton(
                  onPressed: () {
                    deleteTaskScreen(context);
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.red,
                    textStyle: const TextStyle(fontSize: 16),
                  ),
                  child: const Text('SUPPRIMER'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
