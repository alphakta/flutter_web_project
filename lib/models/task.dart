class Task {
  final String id;
  final String title;
  final String description;
  final String status;
  final String assignedUserId;

  Task({
    required this.title,
    required this.description,
    required this.status,
    required this.assignedUserId,
    required this.id,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'status': status,
      'assignedUserId': assignedUserId,
    };
  }
}
