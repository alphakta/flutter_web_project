class User {
  final String name;
  final String lastName;
  final bool isAdmin;
  final String userID;

  User({
    required this.name,
    required this.lastName,
    required this.userID,
    required this.isAdmin,
  });

  get initials {
    return '${name[0]}${lastName[0]}';
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'lastName': lastName,
      'userID': userID,
      'isAdmin': isAdmin,
    };
  }

  static Future<User> fromJson(Map<String, dynamic> data) {
    return Future.value(User(
      name: data['name'],
      lastName: data['lastName'],
      userID: data['userID'],
      isAdmin: data['isAdmin'],
    ));
  }
}
